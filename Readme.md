# Eficia Json Parser

## Description

This project is about taking some json input from old modbus implementation
and convert it to digest lua tables for our newer implementation.

## Why?

Because as we updated our sites, we encountered a lot of old modbus that
couldn't be processed by the tools we were given.

### Known profile list

#### Handled with new modbus scripts

- ISMA_4I_4O_RTU
- ISMA_4O_RTU
- ISMA_4U_4O_RTU
- ISMA_8I_RTU
- ISMA_8U_RTU
- ISMA_MIX18_RTU
- ISMA_MIX38_RTU
- ISMA_4I_4O_TCP
- ISMA_4O_TCP
- ISMA_4U_4O_TCP
- ISMA_8I_TCP
- ISMA_8U_TCP
- ISMA_MIX18_TCP
- ISMA_MIX38_TCP
- MR_AI_8
- MR_AO_4
- MR_DI_10
- MR_DO_4
- MULTIGEM_LITE
- RAIL_350V_CT50_LITE
- RAIL_350V_CT100_LITE
- RAIL_350V_CT200_LITE
- RAIL_350V_CT300_LITE
- RAIL_350V_CT400_LITE

#### Sharepoint available modbus table (Profils harmonysés Guilhem - JS)

- DAIKIN RTD NET
- ETT (all versions)
- LENNOX CLIMATIC (all versions)

## Requirements

- bash shell
- jq


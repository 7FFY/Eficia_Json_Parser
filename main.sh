#!/usr/bin/bash
rm ./OUTPUT.txt
clear

# Ask user path to json_files to process
read -p "Enter json_files path: " USR_PATH

# Search for json json_files in the given path
DIR_CONTENT=$(ls $USR_PATH | grep .json)

# Fetch data from json profile
for json_file in $DIR_CONTENT 
do
	TARGETS_PATH="$USR_PATH"/"$json_file"

	# Fetch manufacturer
	MANUFACTURER=$(jq '.manufacturer' $TARGETS_PATH)

	# Fetch description
	DESCRIPTION=$(jq '.description' $TARGETS_PATH)

	# Concat manufacturer and description as header for result json_file
	HEADER="${MANUFACTURER}_${DESCRIPTION}"

	# Remove quotes from header and replace spaces with underscores
	HEADER=$(echo $HEADER | sed 's/"//g' | sed 's/\s/_/g')
	echo "Processing $HEADER ..."

	# Search term array
	KNOWN_PROFIL=("ISMA" "MR" "MULTI_" "GEM" "RAIL" "RTD" "NET" "ETT" "LENNOX")

	# Iterate over above array, if current profile match one of the keyword in 
	# KNOWN_PROFIL, break otherwise continue
	for item in ${KNOWN_PROFIL[@]}
	do
		REQUEST=$(jq '.description' $TARGETS_PATH | grep --ignore-case $item)
		if [[ "$REQUEST" == "" ]]
		then
			SKIP=0
			continue
		else
			printf "\e[1;31m%s\n\e[0m" "$HEADER is a known profile, skipping..."
			SKIP=1
			break
		fi
	done

	if [[ $SKIP == 0 ]]
	then
		echo "" >> OUTPUT.txt
		# Add profile name above each table
		echo $HEADER >> OUTPUT.txt

		# Fetch how much lines to work with
		ITEM_SUM=$(awk -F : '/name/ {print $2}' $TARGETS_PATH | wc -l)


		# Fetch lines that contain "name" value
		MB_OBJ_NAME_ARRAY=()
		MB_OBJ_NAME=$(jq '.mapping[].name' $TARGETS_PATH)
		# echo -e "Mb name:\n$MB_OBJ_NAME"
		echo -e "$MB_OBJ_NAME" > MB_OBJ_NAME.txt


		# Fetch modbus type (coil, inputregister, register, etc...)
		MB_TYPE=$(jq '.mapping[].type' $TARGETS_PATH)
		# echo -e "Mb memory type:\n$MB_TYPE"
		echo -e "$MB_TYPE" > MB_TYPE.txt

		# Fetch modbus adress
		# MB_ADDR=$(awk -F : '/name/ {print $9}' $USR_PATH"/"$json_file | rm_double_quotes | awk -F , '{print $1}' > mb_obj_addr.txt)
		MB_ADDR=$(jq '.mapping[].address' $TARGETS_PATH)
		# echo -e "Mb addr:\n$MB_ADDR"
		echo -e "$MB_ADDR" > MB_ADDR.txt

		# Fetch multiplier value
		MB_OBJ_VALUE_MULTIPLIER=$(jq '.mapping[].value_multiplier' $TARGETS_PATH)
		# echo -e "Mb value multiplier:\n$MB_OBJ_VALUE_MULTIPLIER"
		echo -e "$MB_OBJ_VALUE_MULTIPLIER" > MB_OBJ_VALUE_MULTIPLIER.txt

		# Fetch bitmask value
		MB_OBJ_VALUE_BITMASK=$(jq '.mapping[].value_bitmask' $TARGETS_PATH)
		# echo -e "Mb bitmask value:\n$MB_OBJ_VALUE_BITMASK"
		echo -e "$MB_OBJ_VALUE_BITMASK" > MB_OBJ_VALUE_BITMASK.txt

		# Fetch read length
		MB_OBJ_READ_LENGHT=$(jq '.mapping[].read_count' $TARGETS_PATH)
		# echo -e "Mb read lenght:\n$MB_OBJ_READ_LENGHT"
		echo -e "$MB_OBJ_READ_LENGHT" > MB_OBJ_READ_LENGHT.txt

		# Fetch read swap
		MB_OBJ_READ_SWAP=$(jq '.mapping[].read_swap' $TARGETS_PATH)
		# echo -e "Mb read swap:\n$MB_OBJ_READ_SWAP"
		echo -e "$MB_OBJ_READ_SWAP" > MB_OBJ_READ_SWAP.txt

		# Fetch knx datatype
		MB_OBJ_KNX_DATATYPE=$(jq '.mapping[].bus_datatype' $TARGETS_PATH)
		# echo -e "KNX_DATATYPE:\n$MB_OBJ_KNX_DATATYPE"
		echo -e "$MB_OBJ_KNX_DATATYPE" > MB_OBJ_KNX_DATATYPE.txt

		# Fetch modbus datatype
		MB_DATATYPE=$(jq '.mapping[].datatype' $TARGETS_PATH)
		# echo -e "MB_DATATYPE:\n$MB_DATATYPE"
		echo -e "$MB_DATATYPE" > MB_DATATYPE.txt

		# Fetch read freq (not applicable)

		# Fetch units
		MB_OBJ_UNIT=$(jq '.mapping[].units' $TARGETS_PATH)
		# echo -e "Units:\n$MB_OBJ_UNIT"
		echo -e "$MB_OBJ_UNIT" > MB_OBJ_UNIT.txt

		# Fetch delta_tolerance_read (not applicable)

		# Fetch tagcache (not applicable)

		# Fetch write mode
		MB_OBJ_WRITABLE=$(jq '.mapping[].writable' $TARGETS_PATH)
		# echo -e "MB_OBJ_WRITABLE:\n$MB_OBJ_WRITABLE"
		echo -e "$MB_OBJ_WRITABLE" > MB_OBJ_WRITABLE.txt

		# if "writable" key in json profile = 1 then
		# assume lasttouch is the wanted mode
		# otherwise leave empty ("nil"?)

		# Fetch write tagcache (not applicable too ambiguous)

		# Fetch enums (not applicable)

		# cat mb_obj_list.txt | while read line; do echo $line;done

		# target=$(jq '32 | tonumber')
		# echo "target = $TARGET"


		# for ((i=1; i<=$ITEM_SUM; i++))
		# do
		# clean_i=$(seq $i $i)
		# typeset -i clean_i=$i
		# clean_i=$(print $clean_i+0)
		# echo $clean_i
		# TARGET=$(jq '.mapping[1]' $TARGETS_PATH)
		# TARGET=$(jq '.mapping[$i]' $TARGETS_PATH)
		# echo "TARGET = $TARGET"
		# done
		# echo -e "TARGET:\n$TARGET"

		# Part 2 (Concat stuff around and output result)
		dq='"'
		for ((i=1; i<=$ITEM_SUM; i++))
		do
			NAME=$(awk -v i=$i 'NR==i' MB_OBJ_NAME.txt)
			TYPE=$(awk -v i=$i 'NR==i' MB_TYPE.txt)
			ADDR=$(awk -v i=$i 'NR==i' MB_ADDR.txt)
			VALUE_MULTIPLIER=$(awk -v i=$i 'NR==i' MB_OBJ_VALUE_MULTIPLIER.txt)
			if [[ $VALUE_MULTIPLIER = "null" ]] 
			then
				VALUE_MULTIPLIER="nil"
			fi
			BITMASK=$(awk -v i=$i 'NR==i' MB_OBJ_VALUE_BITMASK.txt)
			# Replace null with "nil" if bitmask query = null
			if [[ $BITMASK = "null" ]] 
			then
				BITMASK="nil"
			fi
			READ_LENGHT=$(awk -v i=$i 'NR==i' MB_OBJ_READ_LENGHT.txt)
			if [[ $READ_LENGHT = "null" ]] 
			then
				READ_LENGHT="nil"
			fi
			READ_SWAP=$(awk -v i=$i 'NR==i' MB_OBJ_READ_SWAP.txt)
			if [[ $READ_SWAP = "null" ]] 
			then
				READ_SWAP="nil"
			fi
			KNX_DATATYPE=$(awk -v i=$i 'NR==i' MB_OBJ_KNX_DATATYPE.txt)
			# Change $KNX_DATATYPE to the corresponding integer 
			if [[ $KNX_DATATYPE = '"float32"' ]] 
			then
				KNX_DATATYPE="14"
			elif [[ $KNX_DATATYPE = '"float16"' ]]
			then
				KNX_DATATYPE="9"
			elif [[ $KNX_DATATYPE = '"uint32"' ]]
			then
				KNX_DATATYPE="12"
			elif [[ $KNX_DATATYPE = '"uint16"' ]]
			then
				KNX_DATATYPE="7"
			elif [[ $KNX_DATATYPE = '"bool"' ]]
			then
				KNX_DATATYPE="1"
			fi

			MODBUS_DATATYPE=$(awk -v i=$i 'NR==i' MB_DATATYPE.txt)
			if [[ $MODBUS_DATATYPE = "null" ]] 
			then
				MODBUS_DATATYPE="nil"
			fi
			READ_FREQ="nil"
			UNITS=$(awk -v i=$i 'NR==i' MB_OBJ_UNIT.txt)
			if [[ $UNITS = "null" ]] 
			then
				UNITS="nil"
			fi
			DELTA_TOLERANCE="nil"
			TAG_CACHE="nil"
			WRITE_MODE=$(awk -v i=$i 'NR==i' MB_OBJ_WRITABLE.txt)
			# if write mode query = 0 then write mode = nil
			# elif write mode query = 1 then assume write mode = "harmony"
			if [[ $WRITE_MODE = 0 ]] || [[ $WRITE_MODE = "null" ]]
			then
				WRITE_MODE="nil"
			elif [[ $WRITE_MODE = 1 ]] 
			then
				WRITE_MODE='"harmony"'
			elif [[ $WRITE_MODE == "true" ]]
			then
				WRITE_MODE='"harmony"'
			elif [[ $WRITE_MODE == "false" ]]
			then
				WRITE_MODE="nil"
			fi
			# echo "WRITE_MODE=$WRITE_MODE"
			WRITE_TAG="nil"
			ENUMS="nil"

			OUTPUT="{$NAME,$TYPE,$ADDR,$VALUE_MULTIPLIER,$BITMASK,$READ_LENGHT,$READ_SWAP,$KNX_DATATYPE,$MODBUS_DATATYPE,$READ_FREQ,$UNITS,$DELTA_TOLERANCE,$TAG_CACHE,$WRITE_MODE,$WRITE_TAG,$ENUMS},"
			echo $OUTPUT >> OUTPUT.txt
		done
		printf "\e[1;32m%s\n\e[0m" "$HEADER DONE"
	fi

#	{"Feeder#1 - Cumulated Active Energy","inputregister",8022,0.1,nil,nil,nil,14,"uint32",nil,"kWh",nil,"COMPTAGE",nil,nil,nil},
	
done
vim ./OUTPUT.txt

	# Wanted output format:
	# {"MB_OBJ_NAME", "MB_TYPE", "MB_ADDR", "MB_OBJ_VALUE_MULTIPLIER", 
	# "MB_BITMASK", "MB_READ_LENGTH", "MB_READ_SWAP", "KNX_DATATYPE", 
	# "MB_DATATYPE", "READ_FREQ", "UNITS", "DELTA_TOLERANCE", "TAGCACHE", 
	# "WRITE_MODE", "WRITE_TAG", "ENUMS"},

	# Path to material to work with
	# /home/im7ffy/Desktop/Eficia/Test_reprise_modbus_AGEPS/Profil_json



